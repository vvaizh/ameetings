define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/meeting/e_meeting.html'
	, 'forms/ameeting/questions/question/c_question'
],
function (c_binded, tpl, c_question)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' tr.question').click(function (e)
			{
				e.preventDefault();
				var tr = $(e.target).parents('tr.question');
				self.OnOpenQuestion(parseInt(tr.attr('number')));
			});

			$(sel + ' div.cpw-ameeting-meeting-envelope > .close').click(function (e) { e.preventDefault(); self.OnCloseQuestion(); });
		}

		controller.OnOpenQuestion= function(iquestion)
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' div.cpw-ameeting-meeting-envelope').addClass('open');

			console.log(this.model);
			var agenda_question = this.model.Вопросы[iquestion];
			var question =
				{
					Форма_бланка: agenda_question.Форма_бланка
					, Вопрос: agenda_question.Вопрос
				};
			if (agenda_question.Предложенные_варианты)
				question.Предложенные_варианты = agenda_question.Предложенные_варианты;
			if (this.model.Должник)
				question.Должник = this.model.Должник.Наименование;
			if (this.model.Участник_собрания)
			{
				question.Участник_собрания = this.model.Участник_собрания.Наименование;
				question.Сумма_требований = this.model.Участник_собрания.Сумма_требований;
			}
			else
			{
				question.АУ = {
					  Фамилия: this.model.АУ.Фамилия
					, Имя: this.model.АУ.Имя
					, Отчество: this.model.АУ.Отчество
					, Номер_в_ЕФРСБ: this.model.АУ.Номер_в_ЕФРСБ
				};
			}

			var sel_question = sel + ' div.cpw-ameeting-meeting-envelope > div.question';

			var cc_question = c_question();
			cc_question.SetFormContent(question);
			cc_question.Edit(sel_question);
		}

		controller.OnCloseQuestion = function (i)
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' div.cpw-ameeting-meeting-envelope').removeClass('open');
		}

		return controller;
	}
});
