define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/main/e_main.html'
	, 'forms/ameeting/meeting/c_meeting'
],
function (c_binded, tpl, c_meeting)
{
	return function()
	{
		var colSpecs =
		[
			  { label: 'Дата', name: 'Дата', width: 80 }
			, { label: 'Время', name: 'Время', width: 40 }
			, { label: 'Наименование', name: 'Наименование' }
		];

		var colModel = [];
		for (var i = 0; i < colSpecs.length; i++)
		{
			var colSpec = colSpecs[i];
			colModel.push
			({
				width: colSpec.width
				, label: colSpec.label
				, name: colSpec.label
				, stype: colSpec.stype
				, width: colSpec.width
				, align: colSpec.align
				, searchoptions: colSpec.searchoptions
				, sortable: false
			});
		}

		var controller = c_binded(tpl);

		controller.colModel = colModel;

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();

			var self = this;
			$(sel + ' button.to-meeting').click(function (e) { e.preventDefault(); self.OnOpenMeeting(); });
			$(sel + ' div.cpw-ameeting-main-envelope > .close').click(function (e) { e.preventDefault(); self.OnCloseMeeting(); });
		}

		controller.OnOpenMeeting = function ()
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' div.cpw-ameeting-main-envelope').addClass('open');

			var row_id = $(sel + ' table.grid').getGridParam('selrow');
			var imeeting = parseInt(row_id);
			var meeting = this.model[imeeting];

			var sel_meeting = sel + ' div.cpw-ameeting-main-envelope > div.meeting';
			$(sel_meeting).text(meeting.Наименование);

			var cc_meeting = c_meeting();
			cc_meeting.SetFormContent(meeting.Собрание);
			cc_meeting.Edit(sel_meeting);
		}

		controller.OnCloseMeeting = function ()
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' div.cpw-ameeting-main-envelope').removeClass('open');
		}

		controller.OnSelectRow= function()
		{
			this.OnOpenMeeting();
		}

		controller.PrepareGridRow = function (id, row, colSpecs)
		{
			var grid_row =
				{
					id: id
					, data: row
				}
			for (var j = 0; j < colSpecs.length; j++)
			{
				var colSpec = colSpecs[j];
				grid_row[colSpec.label] = !colSpec.prepareValue ? row[colSpec.label] : colSpec.prepareValue(row);
			}
			return grid_row;
		}

		controller.PrepareRowsForGrid = function ()
		{
			var rows = [];
			if (this.model && null != this.model)
			{
				for (var i = 0; i < this.model.length; i++)
				{
					var row = this.PrepareGridRow(i, this.model[i], colSpecs);
					rows.push(row);
				}
			}
			return rows;
		}

		controller.RenderGrid = function ()
		{
			var self = this;
			var sel = this.binding.form_div_selector;
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'local'
				, colModel: this.colModel
				, data: this.PrepareRowsForGrid() // this.model
				, gridview: true
				, recordtext: 'Собрания {0} - {1} из {2}'
				, emptyText: 'Нет собраний, удовлетворяющих условиям фильтрации для просмотра'
				, rownumbers: false
				, rowNum: 10
				, rowList: [5, 10, 15]
				, pager: '#gridPager'
				, viewrecords: true
				, height: 'auto'
				, width: '800'
				, ignoreCase: true
				, onSelectRow: function () { self.OnSelectRow(); }
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		return controller;
	}
});
