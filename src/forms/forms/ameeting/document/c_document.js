﻿define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/document/e_document.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);
		return controller;
	}
});