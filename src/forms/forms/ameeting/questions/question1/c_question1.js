define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/questions/question1/e_question1.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[type="radio"]').change(function () { self.OnChangeRadio(); });
		}

		controller.OnChangeRadio = function ()
		{

		}

		return controller;
	}
});
