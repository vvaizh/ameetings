include ..\..\..\..\..\..\wbt.lib.txt quiet

execute_javascript_stored_lines add_wbt_std_functions

wait_text "Вопрос"

shot_check_png ..\..\shots\01edt.png

wait_click_full_text "Сохранить содержимое формы"
wait_text "Создать новый отчёт"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\test1.json.result.txt

exit