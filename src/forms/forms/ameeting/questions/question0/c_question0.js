define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/questions/question0/e_question0.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		return controller;
	}
});
