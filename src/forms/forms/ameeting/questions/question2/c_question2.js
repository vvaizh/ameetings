define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/questions/question2/e_question2.html'
],
function (c_binded, tpl)
{
	return function ()
	{
		var controller = c_binded(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input[type="radio"]').change(function () { self.OnChangeRadio(); });
			$(sel + ' table tr').click(function (e) { self.OnClickRow(e); });
		}

		controller.OnChangeRadio= function()
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' input[type="text"]').val('');
			this.UpdateUI();
		}

		controller.OnClickRow= function(e)
		{
			var sel = this.binding.form_div_selector;
			var radio = $(e.target).parents('tr').find('input[type="radio"]');
			if (0 != radio.length)
			{
				$(sel + ' input[type="text"]').val('');
				radio.attr('checked', 'checked');
				this.UpdateUI();
			}
		}

		controller.UpdateUI = function ()
		{
			var sel = this.binding.form_div_selector;
			var checked_radio = $(sel + ' input[type="radio"]:checked');
			var selected_value = checked_radio.val();
			$(sel + ' table tr').removeClass('selected');
			checked_radio.parents('tr').addClass('selected');
		}

		return controller;
	}
});
