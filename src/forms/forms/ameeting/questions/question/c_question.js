define([
	  'forms/base/c_binded'
	, 'tpl!forms/ameeting/questions/question/e_question.html'
	, 'forms/ameeting/questions/question0/c_question0'
	, 'forms/ameeting/questions/question1/c_question1'
	, 'forms/ameeting/questions/question2/c_question2'
	, 'tpl!forms/ameeting/questions/question1/p_question1.html'
	, 'tpl!forms/ameeting/questions/question2/p_question2.html'
	, 'tpl!forms/ameeting/questions/question/v_documents.html'
	, 'forms/base/h_times'
	, 'tpl!forms/ameeting/questions/question/p_declaration.html'
	, 'tpl!forms/ameeting/questions/question/p_receipt.html'
],
function (c_binded, tpl, c_question0, c_question1, c_question2, p_question1, p_question2, v_documents, h_times, p_declaration, p_receipt)
{
	return function ()
	{
		var options =
		{
			field_spec:
			{
				question0: { controller: c_question0 }
				,question1: { controller: c_question1 }
				,question2: { controller: c_question2 }
			}
		};

		var controller = c_binded(tpl, options);

		var base_Render = controller.Render;
		controller.Render= function(sel)
		{
			base_Render.call(this, sel);

			switch (this.model.Форма_бланка)
			{
				case '1': $(sel + ' div.blank1').show(); break;
				case '2': $(sel + ' div.blank2').show(); break;
			}

			var self = this;
			$(sel + ' button.sign').click(function (e) { e.preventDefault(); self.OnSign(); });
			$(sel + ' button.select').click(function (e) { e.preventDefault(); self.OnSelect(); });
			$(sel + ' button.send').click(function (e) { e.preventDefault(); self.OnSend(); });

			this.UpdateView();
		}

		controller.OnSelect= function()
		{
			var sel = this.binding.form_div_selector;
			$(sel + ' div.cpw-ameeting-question-envelope').removeClass('sending');
			console.log('remove choosing {');
			$(sel + ' div.cpw-ameeting-question-envelope').addClass('choosing');
			console.log('remove choosing }');
		}

		controller.PrepareContentForAPI= function()
		{
			var sel = this.binding.form_div_selector;

			var q_content = this.GetFormContent();
			if (q_content.Выбранный_вариант)
			{
				var Выбранный_вариант = parseInt(q_content.Выбранный_вариант);
				q_content.Ответ = (Выбранный_вариант < q_content.Предложенные_варианты.length)
					? q_content.Предложенные_варианты[Выбранный_вариант] : q_content.Свой_вариант;
			}
			
			return q_content;
		}

		controller.PrepareContentForUI= function(content_for_api)
		{
			switch (content_for_api.Форма_бланка)
			{
				case '1': return p_question1(content_for_api); break;
				case '2': return p_question2(content_for_api); break;
			}
			return '';
		}

		controller.OnSign= function()
		{
			var sel = this.binding.form_div_selector;

			var q_content = this.PrepareContentForAPI();

			if (!q_content.Ответ)
			{
				alert('Для перехода к голосованию необходимо выбрать вариант ответа.');
			}
			else
			{
				$(sel + ' div.cpw-ameeting-question-envelope').removeClass('choosing');
				$(sel + ' div.cpw-ameeting-question-envelope').addClass('sending');
				var html_content = this.PrepareContentForUI(q_content);
				$(sel + ' div.cpw-ameeting-question-envelope > div.sending > div.content').html(html_content);
			}
		}

		controller.OnSend= function()
		{
			var sel = this.binding.form_div_selector;

			var div_all = $(sel + ' div.cpw-ameeting-question-envelope');
			div_all.removeClass('sending');
			div_all.addClass('sent');

			var content_api = this.PrepareContentForAPI();

			var document=
			{
				  type: 'out'
				, time: this.getTextTime()
				, form:'bulleten'
				, content:
					{
						api: content_api
						, ui: this.PrepareContentForUI(content_api)
					}
				, from:
					{
						Абонент:
							{
								Наименование: content_api.Участник_собрания
							}
					}
			};

			this.model.documents= [document];
			this.model.state = 'sent';

			this.UpdateView();
		}

		controller.UpdateView= function()
		{
			var sel = this.binding.form_div_selector;
			var self = this;
			var sel_documents = ' div.cpw-ameeting-question-envelope > div.documents';
			$(sel_documents).html(v_documents(this.model.documents));
			$(sel_documents + ' button.read').click(function (e) { e.preventDefault(); self.OnRead(); });
			$(sel_documents + ' input.idocument').click(function (e) { self.OnDocument(); });
			if (this.model.documents)
			{
				var idocument = this.model.documents.length - 1;
				var input_sel = sel_documents + ' input.idocument[value="' + idocument + '"]';
				$(input_sel).attr('checked','checked');
				this.OnDocument(idocument);
			}
		}

		controller.OnDocument = function (idocument)
		{
			var sel = this.binding.form_div_selector;
			var sel_documents = ' div.cpw-ameeting-question-envelope > div.documents';
			if (!idocument || null==document)
			{
				var iidocument = $(sel_documents + ' input.idocument:checked').attr('value');
				idocument = parseInt(iidocument);
			}
			var html = this.model.documents[idocument].content.ui;
			$(sel_documents + ' div.content').html(html);
		}

		controller.FindDocument= function(form)
		{
			if (this.model && this.model.documents && 0 != this.model.documents.length)
			{
				for (var i= 0; i<this.model.documents.length; i++)
				{
					var document = this.model.documents[i];
					if (form == document.form)
						return document;
				}
			}
			return null;
		}

		controller.getTextTime= function()
		{
			var d = new Date();
			return d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
		}

		controller.CreateDeclaration= function(bulleten)
		{
			var document =
			{
				type: 'in'
				, time: this.getTextTime()
				, form: 'declaration'
				, content:
					{
						ui: p_declaration(bulleten)
						, api: {}
					}
			};
			return document;
		}

		controller.CreateReceipt = function (bulleten)
		{
			var document =
			{
				type: 'in'
				, time: this.getTextTime()
				, form: 'receipt'
				, content:
					{
						ui: p_receipt(bulleten)
						, api: {Ответ:bulleten.Ответ}
					}
			};
			return document;
		}

		controller.OnRead= function()
		{
			var bulleten = this.FindDocument('bulleten');
			if (bulleten && null != bulleten)
			{
				var declaration = this.FindDocument('declaration');
				if (!declaration || null==declaration)
				{
					this.model.documents.push(this.CreateDeclaration(bulleten.content.api));
				}
				else
				{
					var receipt = this.FindDocument('receipt');
					if (!receipt || null==receipt)
					{
						this.model.documents.push(this.CreateReceipt(bulleten.content.api));
					}
				}
			}
			
			this.UpdateView();
		}

		return controller;
	}
});
