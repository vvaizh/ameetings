define
(
	[
		  'forms/collector'
		, 'forms/ameeting/main/c_main'
		, 'forms/ameeting/questions/question1/c_question1'
		, 'forms/ameeting/questions/question0/c_question0'
		, 'forms/ameeting/questions/question2/c_question2'
		, 'forms/ameeting/questions/question/c_question'
		, 'forms/ameeting/meeting/c_meeting'
		, 'forms/ameeting/document/c_document'
	],
	function (collect)
	{
		return collect([
		  'main'
		, 'question1'
		, 'question0'
		, 'question2'
		, 'question'
		, 'meeting'
		, 'document'
		], Array.prototype.slice.call(arguments,1));
	}
);