define
(
	[
		  'contents/collector'
		, 'txt!forms/ameeting/main/tests/contents/test1.json.txt'
		, 'txt!forms/ameeting/main/tests/contents/test2.json.txt'
		, 'txt!forms/ameeting/questions/question2/tests/contents/test1.json.txt'
		, 'txt!forms/ameeting/questions/question0/tests/contents/test1.json.txt'
		, 'txt!forms/ameeting/questions/question/tests/contents/test1.json.txt'
		, 'txt!forms/ameeting/questions/question/tests/contents/test2.json.txt'
		, 'txt!forms/ameeting/questions/question/tests/contents/test3.json.txt'
		, 'txt!forms/ameeting/questions/question/tests/contents/test4.json.txt'
		, 'txt!forms/ameeting/meeting/tests/contents/test1.json.txt'
		, 'txt!forms/ameeting/meeting/tests/contents/test2.json.txt'
		, 'txt!forms/ameeting/document/tests/contents/test1.json.txt'
	],
	function (collect)
	{
		return collect([
			  'main-test1'
			, 'main-test2'
			, 'question2-test1'
			, 'question0-test1'
			, 'question-test1'
			, 'question-test2'
			, 'question-test3'
			, 'question-test4'
			, 'meeting-test1'
			, 'meeting-test2'
			, 'document-test1'
		], Array.prototype.slice.call(arguments, 1));
	}
);